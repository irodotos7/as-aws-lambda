# as-aws-lambda

AWS Lambda deployment patterns for CQRS and event sourcing implementations

## Goals
To provide slightly opinionated recipes for doing CQRS on aws with event sourcing.

## Before starting

Install [pre-commit](https://pre-commit.com/)

```bash
pre-commit install
```

## Current implementations

### [Simple lambda service](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/tree/main/lambda_service)

![lambda_service](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/raw/main/docs/images/lambda_service.jpg)

[Example](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/blob/main/examples/lambda_service_example.tf)

---
### [Process kinesis events](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/tree/main/lambda_kinesis)

![lambda_service](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/raw/main/docs/lambda_kinesis.jpg)

[Example](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/blob/main/examples/lambda_kinesis_example.tf)

---
### [Pipe kinesis to kinesis](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/tree/main/lambda_kinesis_to_kinesis)

![lambda_service](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/raw/main/docs/lambda_kinesis_to_kinesis.jpg)

[Example](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/blob/main/examples/lambda_kinesis_to_kinesis_example.tf)

---
### [Write to dynamo lambda service](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/tree/main/lambda_write)
In the image below, the Amazon Kinesis Data Streams for DynamoDB is optional
![lambda_write](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/raw/main/docs/images/lambda_write.jpg)

[Example](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/blob/main/examples/lambda_service_example.tf)

---

### [Event sourcing lambda](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/tree/main/lambda_event_source)
In the image below, the Amazon Kinesis Data Streams for DynamoDB is optional
![lambda_event_source](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/raw/main/docs/images/lambda_event_sourcing.jpg)

[Example](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/blob/main/examples/lambda_event_source_example.tf)

---
## [Forward from SQS to SNS](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/tree/main/sqs_to_sns)
![sqs_to_sns](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/raw/main/docs/images/sqs_to_sns.jpg)

[Example](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/blob/main/examples/sqs_to_sns_example.tf)

---
## [Subscribe lambda to SNS](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/tree/main/lambda_sns)
![sqs_to_sns](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/raw/main/docs/images/lambda_sns.jpg)

[Example](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/blob/main/examples/lambda_sns_subscriber_example.tf)

---
## [S3 notifications](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/tree/main/lambda_s3)
![lambda_s3](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/raw/main/docs/images/lambda_s3.jpg)

[Example](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/blob/main/examples/lambda_s3_example.tf)