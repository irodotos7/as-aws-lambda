resource "aws_security_group" "lambda_service_security_group" {
  name        = var.lambda_name
  description = "Security group for ${var.lambda_name}"
  vpc_id      = var.vpc_id
  lifecycle {
    create_before_destroy = true
  }
  tags = var.tags
}

locals {
  security_group_ids = var.security_group_id == null ? [aws_security_group.lambda_service_security_group.id] : [
    aws_security_group.lambda_service_security_group.id, var.security_group_id
  ]
}


resource "aws_security_group_rule" "lambda_outbound_rule" {
  from_port         = 443
  protocol          = "TCP"
  security_group_id = aws_security_group.lambda_service_security_group.id
  to_port           = 443
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Https outbound from private subnets within the lambda security group"
}

resource "aws_lambda_function" "lambda_service" {

  count = local.lambda_no_updates

  filename      = data.archive_file.lambda_package.output_path
  function_name = var.lambda_name
  role          = aws_iam_role.iam_for_lambda_service.arn

  handler = var.lambda_handler

  source_code_hash = data.archive_file.lambda_package.output_base64sha256

  runtime = var.lambda_runtime

  environment {
    variables = var.lambda_environment_variables
  }

  vpc_config {
    subnet_ids         = var.lambda_service_subnets
    security_group_ids = local.security_group_ids
  }

  lifecycle {
    ignore_changes = [source_code_hash, last_modified, filename]
  }
  tags = var.tags

  layers = var.layers
  timeout = var.timeout
}

resource "aws_lambda_function" "lambda_service_updates" {

  count = local.lambda_updates

  filename      = data.archive_file.lambda_package.output_path
  function_name = var.lambda_name
  role          = aws_iam_role.iam_for_lambda_service.arn

  handler = var.lambda_handler

  source_code_hash = data.archive_file.lambda_package.output_base64sha256

  runtime = var.lambda_runtime

  environment {
    variables = var.lambda_environment_variables
  }

  vpc_config {
    subnet_ids         = var.lambda_service_subnets
    security_group_ids = [aws_security_group.lambda_service_security_group.id]
  }
  tags = var.tags

  layers = var.layers
  timeout = var.timeout
}

locals {
  lambda_source     = var.lambda_source_dir == null ? "${path.module}/lambdas/dummy" : var.lambda_source_dir
  lambda_no_updates = var.lambda_source_dir == null ? 1 : 0
  lambda_updates    = var.lambda_source_dir == null ? 0 : 1
}

data "archive_file" "lambda_package" {
  type        = "zip"
  source_dir  = local.lambda_source
  output_path = "${local.lambda_source}.zip"
}

resource "aws_iam_role_policy_attachment" "lambda_service_network_interface" {
  role       = aws_iam_role.iam_for_lambda_service.name
  policy_arn = aws_iam_policy.network_interface_vpc_policy.arn
}

resource "aws_iam_role_policy_attachment" "lambda_service_logging" {
  role       = aws_iam_role.iam_for_lambda_service.name
  policy_arn = aws_iam_policy.lambda_service_logging.arn
}