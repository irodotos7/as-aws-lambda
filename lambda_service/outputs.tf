output "lambda_service_security_group_id" {
  value       = aws_security_group.lambda_service_security_group.id
  description = "The security group id that was created this lambda."
}

output "lambda_service_iam_arn" {
  value       = aws_iam_role.iam_for_lambda_service.arn
  description = "The iam role arn of the lamdba."
}

output "lambda_service_iam_id" {
  value       = aws_iam_role.iam_for_lambda_service.id
  description = "The id of the lamnda iam role."
}

output "lambda_service_function_name" {
  value = local.lambda_updates == 1 ? aws_lambda_function.lambda_service_updates[0].function_name : aws_lambda_function.lambda_service[0].function_name

  description = "The function name of the lambda that was created"
}

output "lambda_service_arn" {
  value       = local.lambda_updates == 1 ? aws_lambda_function.lambda_service_updates[0].arn : aws_lambda_function.lambda_service[0].arn
  description = "The arn of the lambda that was created."
}