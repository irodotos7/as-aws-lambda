module "sns_subscriber" {
  source                       = "../lambda_service"
  lambda_name                  = var.lambda_name
  lambda_handler               = var.lambda_handler
  lambda_environment_variables = var.lambda_environment_variables
  lambda_service_subnets       = var.lambda_service_subnets
  lambda_source_dir            = var.lambda_source_dir
  lambda_runtime               = var.lambda_runtime
  vpc_id                       = var.vpc_id
  security_group_id            = var.security_group_id
  tags                         = var.tags
  timeout                      = var.timeout
}

resource "aws_sns_topic_subscription" "lambda" {
  topic_arn = var.sns_arn
  protocol  = "lambda"
  endpoint  = module.sns_subscriber.lambda_service_arn
}

resource "aws_lambda_permission" "sns_lambda_invocation" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = module.sns_subscriber.lambda_service_function_name
  principal     = "sns.amazonaws.com"
  source_arn    = var.sns_arn
}