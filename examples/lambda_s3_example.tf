module "lambda_s3_example" {
  source             = "../lambda_s3"
  lambda_name        = "s3_events_processor"
  lambda_handler     = "s3_events_processor.handler"
  s3_events_sqs_name = "my_s3_events"
  lambda_environment_variables = {
    MY_ENV_VARIABLE = "my value"
  }
  lambda_service_subnets   = [aws_subnet.main.id]
  vpc_id                   = aws_vpc.main.id
  dead_letters_queue_name  = "my_s3_events_processing_failures"
  dead_letters_lambda_name = "my_failures_notifier"
  enable_consumer          = true
  s3_bucket_id             = aws_s3_bucket.bucket.id
  failure_subject          = "S3 Notification Processing Error"
}