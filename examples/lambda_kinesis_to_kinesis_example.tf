module "lambda_kinesis_to_kinesis_example" {
  source                 = "../lambda_kinesis_to_kinesis"
  lambda_name            = "lambda_kinesis_to_kinesis_example"
  lambda_handler         = "lambda_kinesis_to_kinesis_example.handler"
  aws_kinesis_stream_arn = aws_kinesis_stream.test_stream.arn
  lambda_environment_variables = {
    MY_ENV_VARIABLE = "my value"
  }
  lambda_service_subnets         = [aws_subnet.main.id]
  vpc_id                         = aws_vpc.main.id
  dead_letters_queue_name        = "kinesis_to_kinesis_processor_failures"
  event_source_starting_position = "LATEST"
  enable_consumer                = true
  kinesis_destination_arn        = aws_kinesis_stream.test_output_stream.arn
}