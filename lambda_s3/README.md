## Description

Forward s3 events to sqs and creates a lambda to process those events. The reason an sqs middleware is needed
is so we can configure a dead letters queue when the lambda is failing.

<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_forward_dead_letters"></a> [forward\_dead\_letters](#module\_forward\_dead\_letters) | ../sqs_to_sns | n/a |
| <a name="module_forward_s3_events_to_sqs"></a> [forward\_s3\_events\_to\_sqs](#module\_forward\_s3\_events\_to\_sqs) | ../s3_to_sqs | n/a |
| <a name="module_s3_events_lambda_handler"></a> [s3\_events\_lambda\_handler](#module\_s3\_events\_lambda\_handler) | ../lambda_sqs | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_sqs_queue.processing_failures](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sqs_queue) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_batch_size"></a> [batch\_size](#input\_batch\_size) | The maximum batch size before the lambda is invoked to get sqs messages | `number` | `10` | no |
| <a name="input_batch_window_in_seconds"></a> [batch\_window\_in\_seconds](#input\_batch\_window\_in\_seconds) | The maximum amount to wait before getting the next batch of sqs messages | `number` | `30` | no |
| <a name="input_dead_letters_lambda_name"></a> [dead\_letters\_lambda\_name](#input\_dead\_letters\_lambda\_name) | The name of the lambda to process dead letters | `string` | n/a | yes |
| <a name="input_dead_letters_queue_name"></a> [dead\_letters\_queue\_name](#input\_dead\_letters\_queue\_name) | The name of the dead letters queue to receive processing failure information. | `string` | n/a | yes |
| <a name="input_enable_consumer"></a> [enable\_consumer](#input\_enable\_consumer) | Whether or not the consumer is enabled. Can be used for pausing stream consumption. | `bool` | `true` | no |
| <a name="input_failure_subject"></a> [failure\_subject](#input\_failure\_subject) | The title when a failure is reported to dead letters. | `string` | n/a | yes |
| <a name="input_filter_prefix"></a> [filter\_prefix](#input\_filter\_prefix) | Object key name prefix | `string` | `null` | no |
| <a name="input_filter_suffix"></a> [filter\_suffix](#input\_filter\_suffix) | Object key name suffix | `string` | `null` | no |
| <a name="input_lambda_environment_variables"></a> [lambda\_environment\_variables](#input\_lambda\_environment\_variables) | The environment variables to pass to the lambda runtime. | `map(any)` | `{}` | no |
| <a name="input_lambda_handler"></a> [lambda\_handler](#input\_lambda\_handler) | The lambda handler name. | `string` | n/a | yes |
| <a name="input_lambda_name"></a> [lambda\_name](#input\_lambda\_name) | The name of the lambda to construct | `string` | n/a | yes |
| <a name="input_lambda_runtime"></a> [lambda\_runtime](#input\_lambda\_runtime) | The lambda runtime matching the source code provided in lambda\_source\_dir. | `string` | `"nodejs16.x"` | no |
| <a name="input_lambda_service_subnets"></a> [lambda\_service\_subnets](#input\_lambda\_service\_subnets) | The subnets to assign to the lambda for the vpc configuration. | `list(string)` | `[]` | no |
| <a name="input_lambda_source_dir"></a> [lambda\_source\_dir](#input\_lambda\_source\_dir) | The source directory where all the lambda code is ready to be zipped. If not provided, a dummy lambda is packaged | `string` | `null` | no |
| <a name="input_s3_bucket_id"></a> [s3\_bucket\_id](#input\_s3\_bucket\_id) | The name of the bucket (same bucket as the provided bucket arn) | `string` | n/a | yes |
| <a name="input_s3_events"></a> [s3\_events](#input\_s3\_events) | The s3 events to listen to | `list(string)` | <pre>[<br>  "s3:ObjectCreated:*"<br>]</pre> | no |
| <a name="input_s3_events_sqs_name"></a> [s3\_events\_sqs\_name](#input\_s3\_events\_sqs\_name) | The name of the sqs queue to forward s3 events to | `string` | n/a | yes |
| <a name="input_security_group_id"></a> [security\_group\_id](#input\_security\_group\_id) | A user defined security group for the lambda | `string` | `null` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | (Optional) Key-value map of resource tags to attach to all resources created by this module. | `map(any)` | `null` | no |
| <a name="input_timeout"></a> [timeout](#input\_timeout) | Amount of time your Lambda Function has to run in seconds. Max can be 900 seconds. Defaults to 3. | `number` | `3` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | The vpc id to assign to this lambda. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_dead_letters_sns_queue"></a> [dead\_letters\_sns\_queue](#output\_dead\_letters\_sns\_queue) | The SNS topic ARN to send failure notifications. |
| <a name="output_lambda_service_arn"></a> [lambda\_service\_arn](#output\_lambda\_service\_arn) | The lambda arn if it was created, otherwise null. |
| <a name="output_lambda_service_function_name"></a> [lambda\_service\_function\_name](#output\_lambda\_service\_function\_name) | The function name of the dummy lambda if it was created, otherwise null. |
| <a name="output_lambda_service_iam_arn"></a> [lambda\_service\_iam\_arn](#output\_lambda\_service\_iam\_arn) | The iam role arn of the lamdba. |
| <a name="output_lambda_service_iam_id"></a> [lambda\_service\_iam\_id](#output\_lambda\_service\_iam\_id) | The id of the lamnda iam role. |
| <a name="output_lambda_service_security_group_id"></a> [lambda\_service\_security\_group\_id](#output\_lambda\_service\_security\_group\_id) | The security group id that was created this lambda. |
<!-- END_TF_DOCS -->