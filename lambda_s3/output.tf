output "lambda_service_security_group_id" {
  value       = module.s3_events_lambda_handler.lambda_service_security_group_id
  description = "The security group id that was created this lambda."
}

output "lambda_service_iam_arn" {
  value       = module.s3_events_lambda_handler.lambda_service_iam_arn
  description = "The iam role arn of the lamdba."
}

output "lambda_service_iam_id" {
  value       = module.s3_events_lambda_handler.lambda_service_iam_id
  description = "The id of the lamnda iam role."
}

output "lambda_service_function_name" {
  value       = module.s3_events_lambda_handler.lambda_service_function_name
  description = "The function name of the dummy lambda if it was created, otherwise null."
}

output "lambda_service_arn" {
  value       = module.s3_events_lambda_handler.lambda_service_arn
  description = "The lambda arn if it was created, otherwise null."
}

output "dead_letters_sns_queue" {
  value       = module.forward_dead_letters.forwarding_sns_arn
  description = "The SNS topic ARN to send failure notifications."
}