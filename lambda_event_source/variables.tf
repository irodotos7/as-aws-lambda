variable "dead_letters_queue_name" {
  type        = string
  description = "The name of the dead letters queue to receive processing failure information."
}
variable "dynamo_table_arn" {
  type        = string
  description = "The dynamo table arn that this lambda will be using"
}

/*
    Due to limitations in how terraform module and transitively the aws cli works
    with aws_dynamodb_kinesis_streaming_destination, we need both name and arn
*/
variable "dynamo_table_name" {
  type        = string
  description = "The dynamo table name that this lambda will be using (same table as the provided arn)"
}
variable "lambda_name" {
  type        = string
  description = "The name of the lambda to construct"
}

variable "lambda_environment_variables" {
  type        = map(any)
  default     = {}
  description = "The environment variables to pass to the lambda runtime."
}

variable "lambda_service_subnets" {
  type        = list(string)
  default     = []
  description = "The subnets to assign to the lambda for the vpc configuration."
}

variable "lambda_handler" {
  type        = string
  description = "The lambda handler name."
}

variable "enable_cdc_stream" {
  type        = bool
  default     = false
  description = "Flag whether to capture item level modifications in dynamo db"
}

variable "cdc_stream_name" {
  type        = string
  default     = ""
  description = "The change data capture stream name to get dynamo state change events."
}


variable "vpc_id" {
  type        = string
  description = "The vpc id to assign to this lambda."
}

variable "commands_ack_stream_shards" {
  type        = number
  description = "The number of shards for the dynamo kinesis stream"
  default     = 1
}

variable "enable_consumer" {
  type        = string
  description = "Whether or not the consumer is enabled. Can be used for pausing stream consumption."
}

variable "aws_kinesis_stream_arn" {
  type        = string
  description = "The kinesis stream arn for the lambda to consume and process events from."
}
variable "event_source_starting_position" {
  type        = string
  description = "The starting position of the event source mapping for the dynamodb kinesis stream as defined in https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping#starting_position."
}

variable "event_source_starting_at_timestamp" {
  type        = string
  default     = null
  description = "The timestamp position of the kinesis iterator for the dynamodb kinesis stream as defined in https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping#starting_position_timestamp."
}

variable "read_batch_size" {
  type        = number
  default     = 100
  description = "The largest number of records that Lambda will retrieve from your event source at the time of invocation as defined in https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping#batch_size"
}

variable "bisect_batch_on_function_error" {
  type        = bool
  default     = true
  description = "If the function returns an error, split the batch in two and retry."
}

variable "maximum_batching_window_in_seconds" {
  type        = number
  default     = 2
  description = "The maximum amount of time to gather records before invoking the function, in seconds as defined in https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping#maximum_batching_window_in_seconds"
}

variable "maximum_retry_attempts" {
  type        = number
  default     = 2
  description = "The number of retries as defined in https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping#maximum_retry_attempts"
}

variable "lambda_source_dir" {
  type        = string
  description = "The source directory where all the lambda code is ready to be zipped. If not provided, a dummy lambda is packaged"
  default     = null
}

variable "lambda_runtime" {
  type        = string
  description = "The lambda runtime matching the source code provided in lambda_source_dir."
  default     = "nodejs16.x"
}

variable "security_group_id" {
  type        = string
  description = "A user defined security group for the lambda"
  default     = null
}

variable "tags" {
  type        = map(any)
  description = "(Optional) Key-value map of resource tags to attach to all resources created by this module."
  default     = null
}

variable "timeout" {
  type        = number
  description = "Amount of time your Lambda Function has to run in seconds. Max can be 900 seconds. Defaults to 3."
  default     = 3
}
