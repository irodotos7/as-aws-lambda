# Lambda event source

Pipes events from kinesis to dynamo via a lambda function. Failures are emitted 
to an sns dead letter queue.
<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_lambda_processor"></a> [lambda\_processor](#module\_lambda\_processor) | ../lambda_write | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_iam_role_policy.kinesis_to_lambda_read](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.publish_processing_failures](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_lambda_event_source_mapping.kinesis_to_lambda](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping) | resource |
| [aws_sns_topic.processing_failures](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_kinesis_stream_arn"></a> [aws\_kinesis\_stream\_arn](#input\_aws\_kinesis\_stream\_arn) | The kinesis stream arn for the lambda to consume and process events from. | `string` | n/a | yes |
| <a name="input_bisect_batch_on_function_error"></a> [bisect\_batch\_on\_function\_error](#input\_bisect\_batch\_on\_function\_error) | If the function returns an error, split the batch in two and retry. | `bool` | `true` | no |
| <a name="input_cdc_stream_name"></a> [cdc\_stream\_name](#input\_cdc\_stream\_name) | The change data capture stream name to get dynamo state change events. | `string` | `""` | no |
| <a name="input_commands_ack_stream_shards"></a> [commands\_ack\_stream\_shards](#input\_commands\_ack\_stream\_shards) | The number of shards for the dynamo kinesis stream | `number` | `1` | no |
| <a name="input_dead_letters_queue_name"></a> [dead\_letters\_queue\_name](#input\_dead\_letters\_queue\_name) | The name of the dead letters queue to receive processing failure information. | `string` | n/a | yes |
| <a name="input_dynamo_table_arn"></a> [dynamo\_table\_arn](#input\_dynamo\_table\_arn) | The dynamo table arn that this lambda will be using | `string` | n/a | yes |
| <a name="input_dynamo_table_name"></a> [dynamo\_table\_name](#input\_dynamo\_table\_name) | The dynamo table name that this lambda will be using (same table as the provided arn) | `string` | n/a | yes |
| <a name="input_enable_cdc_stream"></a> [enable\_cdc\_stream](#input\_enable\_cdc\_stream) | Flag whether to capture item level modifications in dynamo db | `bool` | `false` | no |
| <a name="input_enable_consumer"></a> [enable\_consumer](#input\_enable\_consumer) | Whether or not the consumer is enabled. Can be used for pausing stream consumption. | `string` | n/a | yes |
| <a name="input_event_source_starting_at_timestamp"></a> [event\_source\_starting\_at\_timestamp](#input\_event\_source\_starting\_at\_timestamp) | The timestamp position of the kinesis iterator for the dynamodb kinesis stream as defined in https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping#starting_position_timestamp. | `string` | `null` | no |
| <a name="input_event_source_starting_position"></a> [event\_source\_starting\_position](#input\_event\_source\_starting\_position) | The starting position of the event source mapping for the dynamodb kinesis stream as defined in https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping#starting_position. | `string` | n/a | yes |
| <a name="input_lambda_environment_variables"></a> [lambda\_environment\_variables](#input\_lambda\_environment\_variables) | The environment variables to pass to the lambda runtime. | `map(any)` | `{}` | no |
| <a name="input_lambda_handler"></a> [lambda\_handler](#input\_lambda\_handler) | The lambda handler name. | `string` | n/a | yes |
| <a name="input_lambda_name"></a> [lambda\_name](#input\_lambda\_name) | The name of the lambda to construct | `string` | n/a | yes |
| <a name="input_lambda_runtime"></a> [lambda\_runtime](#input\_lambda\_runtime) | The lambda runtime matching the source code provided in lambda\_source\_dir. | `string` | `"nodejs16.x"` | no |
| <a name="input_lambda_service_subnets"></a> [lambda\_service\_subnets](#input\_lambda\_service\_subnets) | The subnets to assign to the lambda for the vpc configuration. | `list(string)` | `[]` | no |
| <a name="input_lambda_source_dir"></a> [lambda\_source\_dir](#input\_lambda\_source\_dir) | The source directory where all the lambda code is ready to be zipped. If not provided, a dummy lambda is packaged | `string` | `null` | no |
| <a name="input_maximum_batching_window_in_seconds"></a> [maximum\_batching\_window\_in\_seconds](#input\_maximum\_batching\_window\_in\_seconds) | The maximum amount of time to gather records before invoking the function, in seconds as defined in https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping#maximum_batching_window_in_seconds | `number` | `2` | no |
| <a name="input_maximum_retry_attempts"></a> [maximum\_retry\_attempts](#input\_maximum\_retry\_attempts) | The number of retries as defined in https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping#maximum_retry_attempts | `number` | `2` | no |
| <a name="input_read_batch_size"></a> [read\_batch\_size](#input\_read\_batch\_size) | The largest number of records that Lambda will retrieve from your event source at the time of invocation as defined in https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping#batch_size | `number` | `100` | no |
| <a name="input_security_group_id"></a> [security\_group\_id](#input\_security\_group\_id) | A user defined security group for the lambda | `string` | `null` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | (Optional) Key-value map of resource tags to attach to all resources created by this module. | `map(any)` | `null` | no |
| <a name="input_timeout"></a> [timeout](#input\_timeout) | Amount of time your Lambda Function has to run in seconds. Max can be 900 seconds. Defaults to 3. | `number` | `3` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | The vpc id to assign to this lambda. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_dead_letters_sns_queue"></a> [dead\_letters\_sns\_queue](#output\_dead\_letters\_sns\_queue) | The arn of the SNS dead letters queue (contains event information of processing failures). |
| <a name="output_dead_letters_sns_queue_id"></a> [dead\_letters\_sns\_queue\_id](#output\_dead\_letters\_sns\_queue\_id) | The id of the SNS dead letters queue. |
| <a name="output_dead_letters_sns_queue_name"></a> [dead\_letters\_sns\_queue\_name](#output\_dead\_letters\_sns\_queue\_name) | The name of the SNS dead letters queue |
| <a name="output_lambda_service_arn"></a> [lambda\_service\_arn](#output\_lambda\_service\_arn) | The lambda arn if it was created, otherwise null. |
| <a name="output_lambda_service_function_name"></a> [lambda\_service\_function\_name](#output\_lambda\_service\_function\_name) | The function name of the dummy lambda if it was created, otherwise null. |
| <a name="output_lambda_service_iam_arn"></a> [lambda\_service\_iam\_arn](#output\_lambda\_service\_iam\_arn) | The iam role arn of the lamdba. |
| <a name="output_lambda_service_iam_id"></a> [lambda\_service\_iam\_id](#output\_lambda\_service\_iam\_id) | The id of the lamnda iam role. |
| <a name="output_lambda_service_security_group_id"></a> [lambda\_service\_security\_group\_id](#output\_lambda\_service\_security\_group\_id) | The security group id for this lambda. |
| <a name="output_persist_commands_ack_stream_arn"></a> [persist\_commands\_ack\_stream\_arn](#output\_persist\_commands\_ack\_stream\_arn) | n/a |
<!-- END_TF_DOCS -->