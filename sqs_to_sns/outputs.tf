output "forwarding_sns_arn" {
  value       = aws_sns_topic.to_forward.arn
  description = "The sns topic that sqs messages are forwarded to."
}