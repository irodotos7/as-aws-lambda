var AWS = require("aws-sdk");

exports.handler = async (event, context, callback) => {

    const SUBJECT = process.env.SNS_SUBJECT;
    const SNS_ARN = process.env.SNS_ARN;

    const sns = new AWS.SNS();
    const params = {
        Subject: SUBJECT,
        TopicArn: SNS_ARN
    }

    return Promise.all(
        event.Records.map(
            record => sns.publish({...params, Message: record.body}).promise()
        )
    );
}