# CONTRIBUTING

Thank you for your interest in this project!

## I want to ask a question

Please email [open-source@arcticshores.com](mailto:open-source@arcticshores.com) and our team will put you in touch with the developer(s) responsible for this project.

## I want to discuss an idea / file a bug / suggest an enhancement

Please [file an issue](https://gitlab.com/arctic-shores-open-source/as-aws-lambda/-/issues/new) to start a discussion.

## I want to provide a patch / pull request

We warmly welcome code contributions! If you would like to contribute code, please submit a pull request.

For a pull request to be accepted we ask you to sign a Contributor Licence Agreement:

- If you're contributing as an individual, please choose the [Individual Contributor Licence Agreement](https://eu.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=71658cf6-493e-44e9-bb64-b3a53accb0d6&env=eu&acct=8a8fa6da-6d59-44be-8a6d-feff48b0577d&v=2).
- If you're contributing on behalf of a company, please choose the [Entity Contributor Licence Agreement](https://eu.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=a95d77b0-4c12-477d-b593-fd1bfe8ae0f0&env=eu&acct=8a8fa6da-6d59-44be-8a6d-feff48b0577d&v=2).

The agreement is a [FSFE recommended agreement](https://fsfe.org/activities/fla/fla.en.html) generated via the [Contributor Agreements](https://contributoragreements.org/) service.

## Code of Conduct

Contributors are kindly asked to read and follow our [Code of Conduct](CODE_OF_CONDUCT.md).
